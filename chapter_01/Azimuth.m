function Alfa=Azimuth(dx,dy)
%dx和dy都是终点-起点的坐标分量差
if abs(dy)<1.0e-10 & dx>0
    Alfa=0.0;
elseif abs(dy)<1.0e-10 & dx<0
    Alfa=pi;
elseif abs(dx)<1.0e-10 & dy>0
    Alfa=pi/2;
elseif abs(dx)<1.0e-10 & dy<0
    Alfa=pi*3/2;
else
    Alfa=atan(dy/dx);
    if dx>0 & dy<0
        Alfa=2*pi+Alfa;
    elseif dx<0 
        Alfa=Alfa+pi;
    else
        Alfa=Alfa;
    end
end
