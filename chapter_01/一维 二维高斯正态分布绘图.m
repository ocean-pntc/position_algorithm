x=-1:0.001:7;
y=normpdf(x,3,1);
k1=find(x==0);
k2=find(x==6);
k3=find(x==1);
k4=find(x==5);
k5=find(x==2);
k6=find(x==4);

figure;
hold on;
plot(x,y,'r');
plot([0 0],[-0.2 y(k1)],'k');
plot([6 6],[-0.2 y(k2)],'k');
plot([1 1],[-0.2 y(k3)],'g');
plot([5 5],[-0.2 y(k4)],'g');
plot([2 2],[-0.2 y(k5)],'b');
plot([4 4],[-0.2 y(k6)],'b');
title('一维正态分布条件概率密度函数曲线');
xlim([-2 8]);
ylim([-0.1 0.5]);

x=-20:0.5:20;
y=-20:0.5:20;
u1 = 2;          %均值
u2 = 0;        
sigma1 = 5;      %方差
sigma2 = 2;
rou = 0.5;     %相关系数
mu=[-1,2];
[X,Y]=meshgrid(x,y); % 产生网格数据并处理
p = 1/(2*pi*sigma1*sigma2*sqrt(1-rou*rou)).*exp(-1/(2*(1-rou^2)).*[(X-u1).*(X-u1)/(sigma1*sigma1)-2*rou*(X-u1).*(Y-u2)/(sigma1*sigma2)+(Y-u2).*(Y-u2)/(sigma2*sigma2)]);
figure(2)
surf(X,Y,p)
shading interp
colorbar
title('二维正态分布条件概率密度函数曲线');
