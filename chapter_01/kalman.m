function  [Xp,Qp,Xe,Qe]=kalman(Xe,Qe,F,I,Qw,B,L,R,type)
%输入：
%     Xe，Qe为上一时刻的估计结果
%     F，I和Qw为动力学模型参数
%     B，L和R为观测模型参数
%     type为计算模式，如果type=0则只预测不估计，其他值预测并且估计

%输出:
%     Xp,Qp为预测结果，Xe,Qe为估计结果
%     如不分析预测结果，Xp和Qp可以不输出

%预测
Xp=F*Xe;
Qp=F*Qe*F'+Qw;
%估计
if type==0
    %type=0不做估计
else
    V=B*Xp-L;
    K=Qp*B'*pinv(B*Qp*B'+R);
    Qe=(I-K*B)*Qp*(I-B'*K')+K*R*K';
    Xe=Xp-K*V;
end
