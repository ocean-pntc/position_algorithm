function  [I,F]=Dynamic(dimension,type,dt)

I=eye(dimension);
switch type
    case 'CP'  %静态模型
        F=I;
    case 'CV'  %常速度模型
        F=[I I*dt;zeros(size(I)) I];
    case 'CA'  %常加速度模型
        F=[I I*dt I*dt^2/2;zeros(size(I)) I I*dt;zeros(size(I)) zeros(size(I)) I];
    otherwise
        F=0;
end
I=eye(size(F,2));
% [X,Q,V]=kalman(I,F,Q,Qw,B,L,X,R)