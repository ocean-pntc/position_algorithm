clear
%   1       2       3       4
Px=[0       10      10      0];
Py=[0       0       5       5];
D =[4.535   6.334   6.736   5.054];

P0x=4;  P0y=2;

for i=1:4
    Di=norm([Px(i)-P0x,Py(i)-P0y]);
    B(i,1)=(P0x-Px(i))/Di;
    B(i,2)=(P0y-Py(i))/Di;
    L(i,1)=D(i)-Di;
end;

[dx,sigma0]=LS(B,L)

P0x=P0x+dx(1)
P0y=P0y+dx(2)
