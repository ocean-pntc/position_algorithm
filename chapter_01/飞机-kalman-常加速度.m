clear
Px=[4050.77  3199.86  2449.66  1800.50  1249.94  799.97  450.35  200.11   50.15  -0.40];
Py=[ 243.20   192.28   146.42   107.29    75.86   48.42   26.61   12.03    3.03   0.13];
Ph=[ 161.91   127.38    97.45    72.02    50.32   32.00   17.95    7.86    1.31   0.03];
Vx=[-900.02  -800.08  -700.03  -599.97  -499.97 -399.94 -300.02 -200.10 -100.02   0.01];
Vy=[ -53.96   -47.99   -42.02   -36.02   -30.00  -23.99  -18.00  -11.99   -6.04   0.08];
Vh=[ -35.97   -32.02   -27.94   -23.98   -20.06  -15.97  -12.03   -7.98   -4.06  -0.05];

PV=[Px;Py;Ph;Vx;Vy;Vh];
%设置初始位置、速度及对应方差
PV0=[4000 250 160 -900 -50 -30]';Qe=100^2*eye(9);
%设置观测噪声（误差）
R=[1^2*eye(3) zeros(3);zeros(3) 0.1^2*eye(3)];
%目前不涉及随机游走，所以随机游走量为0
Qw=zeros(9);
%设置初始坐标修正量为0
Xe=[PV0;zeros(3,1)];
%设置动力学模型
[I,F]=Dynamic(3,'CA',1);
B=[eye(6) zeros(6,3)];

PVp=[];PVe=[];
for i=1:size(PV,2)
    [Xp,Qp,Xe,Qe]=kalman(Xe,Qe,F,I,Qw,B,PV(:,i),R,1);
    PVp=[PVp Xp];
    PVe=[PVe Xe];
end;
[Xp,Qp,Xe,Qe]=kalman(Xe,Qe,F,I,Qw,B,PV(i),R,0);
PVp=[PVp Xp];


t=10:-1:1;
hold on;
subplot(2,1,1);
plot(t,PV(1,:)-PVp(1,1:end-1),'r^',t,PV(2,:)-PVp(2,1:end-1),'g*',t,PV(3,:)-PVp(3,1:end-1),'bo');
legend('XPp','YPp','ZPp');
box on;grid on
xlabel('时刻（单位：秒）');ylabel('位置误差（单位：米）');
subplot(2,1,2);
plot(t,PV(1,:)-PVe(1,:),'r^',t,PV(2,:)-PVe(2,:),'r*',t,PV(3,:)-PVe(3,:),'ro');
legend('XPe','YPe','ZPe');
box on;grid on
xlabel('时刻（单位：秒）');ylabel('位置误差（单位：米）');
title('常加速度模型kalman滤波估计位置误差');
hold off;

hold on;
subplot(2,1,1);
plot(t,PV(4,:)-PVp(4,1:end-1),'r^',t,PV(5,:)-PVp(5,1:end-1),'g*',t,PV(6,:)-PVp(6,1:end-1),'bo');
legend('XVp','YVp','ZVp');
box on;grid on
xlabel('时刻（单位：秒）');ylabel('位置误差（单位：米/秒）');
subplot(2,1,2);
plot(t,PV(4,:)-PVe(4,:),'r^',t,PV(5,:)-PVe(5,:),'r*',t,PV(6,:)-PVe(6,:),'ro');
legend('XVe','YVe','ZVe');
box on;grid on
xlabel('时刻（单位：秒）');ylabel('位置误差（单位：米/秒）');
title('常加速度模型kalman滤波估计速度误差');
hold off;