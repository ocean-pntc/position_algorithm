function [dx,sigma0]=LS(B,L)

dx=inv(B'*B)*B'*L
V=B*dx-L
sigma0=sqrt(V'*V/(size(B,1)-size(B,2)))