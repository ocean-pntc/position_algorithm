clear
T=[25.0 25.1 25.0 24.7 24.2 23.5 22.6 21.5 20.2 18.7 17.0];

%设置初始温度、温度变化率及对应方差
T0=[28 0]';Qe=5^2*eye(2);
%设置观测噪声（误差）
R=0.1^2;
%目前不涉及随机游走，所以随机游走量为0
Qw=zeros(2);
%设置初始坐标修正量为0
Xe=T0;
%设置动力学模型
[I,F]=Dynamic(1,'CV',1);
B=[1 0];

Tp=[];Te=[];type=1;
for i=1:size(T,2)
    [Xp,Qp,Xe,Qe]=kalman(Xe,Qe,F,I,Qw,B,T(i),R,type);
    Tp=[Tp Xp];
    Te=[Te Xe];
end;
[Xp,Qp,Xe,Qe]=kalman(Xe,Qe,F,I,Qw,B,T(i),R,0);
Tp=[Tp Xp];


t=12:23;
figure;
hold on;
plot(t(1:end-1),T,'b^',t,Tp(1,:),'go',t(1:end-1),Te(1,:),'r*');
legend('观测值','预测值','估计值');
xlabel('时刻（单位：小时）');ylabel('温度（单位：摄氏度）');
title('常速度模型kalman滤波温度估计');
box on;grid on
hold off;

figure;
hold on;
plot(t,Tp(2,:),'go',t(1:end-1),Te(2,:),'r*');
legend('预测温速','估计温速');
xlabel('时刻（单位：小时）');ylabel('温度（单位：摄氏度）');
title('常速度模型kalman滤波温度估计');
box on;grid on
hold off;