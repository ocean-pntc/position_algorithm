clear;
P1x=500;    P1y=1000;
P2x=10000;  P2y=5000;
P3x=2000;   P3y=10000;
P0x=6000;   P0y=6000;

D01=7433.940;
D02=4123.232;
D03=5657.767;

a1=(P0x-P1x)/D01;   b1=(P0y-P1y)/D01;    l1=D01-norm([P0x-P1x,P0y-P1y]);
a2=(P0x-P2x)/D02;   b2=(P0y-P2y)/D02;    l2=D02-norm([P0x-P2x,P0y-P2y]);
a3=(P0x-P3x)/D03;   b3=(P0y-P3y)/D03;    l3=D03-norm([P0x-P3x,P0y-P3y]);

aa=a1^2+a2^2+a3^2;
bb=b1^2+b2^2+b3^2;
ab=a1*b1+a2*b2+a3*b3;
al=a1*l1+a2*l2+a3*l3;
bl=b1*l1+b2*l2+b3*l3;

dx=(aa*al-ab*bl)/(ab^2-aa*bb)
dy=(aa*bl-ab*al)/(ab^2-aa*bb)

P0x=P0x+dx
P0y=P0y+dy