clear;
a=127.911;b=100.596;c=125.578;
xA=10;yA=-100;
xB=0;yB=0;
xC=40;yC=100;

%Triangle ABP
alpha_AB=(180/pi)*Azimuth((xB-xA),(yB-yA));
S_AB=norm([xB-xA,yB-yA]);
angle_A=(180/pi)*acos((S_AB^2+a^2-b^2)/(2*a*S_AB));

alpha_AP=alpha_AB-angle_A;

xP1=xA+a*cos(alpha_AP*(pi/180));
yP1=yA+a*sin(alpha_AP*(pi/180));

%Triangle BCP
alpha_CB=(180/pi)*Azimuth((xB-xC),(yB-yC));
S_CB=norm([xB-xC,yB-yC]);
angle_C=(180/pi)*acos((S_CB^2+c^2-b^2)/(2*c*S_CB));

alpha_CP=alpha_CB+angle_C;

xP2=xC+c*cos(alpha_CP*(pi/180));
yP2=yC+c*sin(alpha_CP*(pi/180));

delta_x=xP1-xP2
delta_y=yP1-yP2