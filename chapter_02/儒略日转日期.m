clear;
%2022年6月15日1时10分20秒的儒略日
jd=2459745.548842593;

b=double(floor(jd+0.5)+1537);
c=double(floor((b-122.1)/365.25));
d=double(floor(365.25*c));
e=double(floor((b-d)/30.6001));

hour=double(24*(jd+0.5-floor(jd+0.5)));
minute=floor((hour-floor(hour))*60);
second=((hour-floor(hour))*60-minute)*60;
hour=floor(hour);
day=b-d-floor(30.6001*e);
month=e-1-12*floor(e/14);
year=c-4715-floor((7+month)/10);