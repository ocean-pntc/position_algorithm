clear
%以CGCS2000坐标框架参数计算
A=6378137.0; %地球长半轴长度，单位：米
Alfa=1.0/298.257222101;%偏心率
E2=2*Alfa-(Alfa)^2;
       
%以山东科技大学内某点坐标为例
X=-2591041.86108;
Y=4469361.88495;
Z=3728197.94583;
   
%由三维坐标计算经纬度和高程
R=sqrt(X*X+Y*Y);
B0=atan2(Z,R);
while 1  %迭代计算
    N=A/sqrt(1-E2*sin(B0)*sin(B0));
    B=atan2(Z+N*E2*sin(B0),R);
    if abs(B-B0)<1e-12  %迭代退出条件，精度优于毫米
        break;
    end;
    B0=B;
    L=atan2(Y,X);
    H=R/cos(B)-N;
end;
%显示结果，并将经纬度的单位转换成°
B*180/pi
L*180/pi
H
%将计算出来的经纬度和高程，转换回去
N=A/sqrt(1.0-E2*sin(B)*sin(B));
X1=(N+H)*cos(B)*cos(L);
Y1=(N+H)*cos(B)*sin(L);
Z1=(N*(1-E2)+H)*sin(B);
%计算转换误差
dx=X-X1
dy=Y-Y1
dz=Z-Z1