clear
year=2022;
month=6;
day=15;
hour=1;
minute=10;
second=20;
%此方法适用于1900-2100这200年时间
if month <= 2
   year = year-1; 
   month = month+12;
end
%将时分秒转化为小时
hour=hour+minute/60+second/3600;
jd = floor(365.25*year)+floor(30.6001*(month+1))+day+hour/24+1720981.5;