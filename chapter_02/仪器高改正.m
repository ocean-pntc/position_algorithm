clear
%已知接收机所在坐标Pr
Xr=4231162.7880;Yr=-332746.9200;Zr=4745130.6890;
%接收机点经纬度BLH：
Br=48.380490678635212*pi/180;%转化为弧度
Lr=-4.496597617899513*pi/180;%转化为弧度
Hr=65.805;%单位：m

R=zeros(3);
R(1,1)=-sin(Br)*cos(Lr);
R(1,2)=-sin(Br)*sin(Lr);
R(1,3)= cos(Br);
R(2,1)=-sin(Lr);
R(2,2)=cos(Lr);
R(2,3)=0;
R(3,1)=cos(Br)*cos(Lr);
R(3,2)=cos(Br)*sin(Lr);
R(3,3)=sin(Br);
%假设仪器高为1.00米，则固定点到天线所在位置的NEU方向分量为
NEU=[0 0 1.00]';%分别代表北、东、天方向
xyz=inv(R)*NEU;%将NEU方向分量
%由固定点P0+xyz=Pr可得：
P0=[Xr Yr Zr]'-xyz