clear
% Written by Kai Borre
% May 20, 1996
%2022年6月15日1时10分20秒的儒略日
jd=2459745.548842593;

a = floor(jd+.5);
b = a+1537;
c = floor((b-122.1)/365.25);
e = floor(365.25*c);
f = floor((b-e)/30.6001);
d = b-e-floor(30.6001*f)+rem(jd+.5,1);
day_of_week = rem(floor(jd+.5),7);
week = floor((jd-2444244.5)/7);
% We add +1 as the GPS week starts at Saturday midnight
sec_of_week = (rem(d,1)+day_of_week+1)*86400;