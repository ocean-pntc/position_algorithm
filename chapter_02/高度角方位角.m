clear
%已知观察点坐标和卫星坐标，计算卫星相对于观察点的距离，高度角和方位角
Xr=4231162.7880;Yr=-332746.9200;Zr=4745130.6890;
Xs=25571052.8243;Ys=-7554759.9508;Zs=-2719138.7257;
%观察点的经纬度BLH：
Br=48.380490678635212*pi/180;%转化为弧度
Lr=-4.496597617899513*pi/180;%转化为弧度
Hr=65.805;%单位：m

R=zeros(3);
R(1,1)=-sin(Br)*cos(Lr);
R(1,2)=-sin(Br)*sin(Lr);
R(1,3)= cos(Br);

R(2,1)=-sin(Lr);
R(2,2)=cos(Lr);
R(2,3)=0;

R(3,1)=cos(Br)*cos(Lr);
R(3,2)=cos(Br)*sin(Lr);
R(3,3)=sin(Br);

Dx=R(1,1)*(Xs-Xr)+R(1,2)*(Ys-Yr)+R(1,3)*(Zs-Zr);
Dy=R(2,1)*(Xs-Xr)+R(2,2)*(Ys-Yr)+R(2,3)*(Zs-Zr);
Dz=R(3,1)*(Xs-Xr)+R(3,2)*(Ys-Yr)+R(3,3)*(Zs-Zr);

S=sqrt(Dx*Dx+Dy*Dy+Dz*Dz);%卫星与观察点的距离
A=Azimuth(Dx,Dy)*180/pi;%实现投影到平面上与北方向夹角
E=atan(Dz/sqrt(Dx*Dx+Dy*Dy))*180/pi;%实现与地平线夹角